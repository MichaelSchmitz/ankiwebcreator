﻿using System.Text.Json.Serialization;

namespace AnkiDB.Models.JSON
{
    /// <summary>
    /// Based on https://github.com/ankidroid/Anki-Android/wiki/Database-Structure#decks-jsonobjects
    /// </summary>
    public class Deck
    {
        [JsonPropertyName("id")]
        public long DeckID { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("extendedRev")]
        /// <value>
        /// Potentially absent, in this case it's considered to be 10 by aqt.customstudy
        /// </value>
        public int ExtendedReviewCardLimit { get; set; }

        [JsonPropertyName("usn")]
        /// <value>
        /// used to figure out diffs when syncing. 
        /// value of -1 indicates changes that need to be pushed to server.
        /// usn &lt; server usn indicates changes that need to be pulled from server.
        /// </value>
        public int UpdateSequenceNumber { get; set; }

        [JsonPropertyName("collapsed")]
        public bool Collapsed { get; set; }

        [JsonPropertyName("browserCollapsed")]
        public bool BrowserCollapsed { get; set; }

        [JsonPropertyName("newToday")]
        /// <value>
        /// Array of size two
        /// [0] number of days that have passed between the collection was created and the deck was last updated
        /// [1] number of cards seen today in this deck minus the number of new cards in custom study today
        /// BEWARE, it's changed in anki.sched(v2).Scheduler._updateStats and anki.sched(v2).Scheduler._updateCutoff.update  but can't be found by grepping 'newToday', because it's instead written as type+"Today" with type which may be new/rev/lrnToday    
        /// </value>
        public int[] NewToday { get; set; }
            
        [JsonPropertyName("revToday")]
        /// <value>
        /// Array of size two
        /// [0] number of days that have passed between the collection was created and the deck was last updated
        /// [1] number of cards seen today in this deck minus the number of new cards in custom study today
        /// BEWARE, it's changed in anki.sched(v2).Scheduler._updateStats and anki.sched(v2).Scheduler._updateCutoff.update  but can't be found by grepping 'newToday', because it's instead written as type+"Today" with type which may be new/rev/lrnToday    
        /// </value>
        public int[] RevisionsToday { get; set; }

        [JsonPropertyName("lrnToday")]
        /// <value>
        /// Array of size two
        /// [0] number of days that have passed between the collection was created and the deck was last updated
        /// [1] number of cards seen today in this deck minus the number of new cards in custom study today
        /// BEWARE, it's changed in anki.sched(v2).Scheduler._updateStats and anki.sched(v2).Scheduler._updateCutoff.update  but can't be found by grepping 'newToday', because it's instead written as type+"Today" with type which may be new/rev/lrnToday    
        /// </value>
        public int[] LearnedToday { get; set; }

        [JsonPropertyName("timeToday")]
        /// <value>
        /// Array of size two, somehow used for custom study
        /// </value>
        public int[] TimeToday { get; set; }

        [JsonPropertyName("dyn")]
        /// <value>
        /// 1 for dynamic/ filtered deck, else 0
        /// </value>
        public int Dynamic { get; set; }

        [JsonPropertyName("extendedNew")]
        /// <value>
        /// Potentially absent, in this case it's considered to be 10 by aqt.customstudy
        /// </value>
        public int ExtendedNewCardLimit { get; set; }

        [JsonPropertyName("conf")]
        /// <value>
        /// ID from OptionGroup (in DeckConfiguration), absent if dynamic/ filtered deck
        /// </value>
        public long ConfigurationID { get; set; }

        [JsonPropertyName("mod")]
        public long LastModificationTime { get; set; }

        [JsonPropertyName("desc")]
        public string Description { get; set; }
    }
}
