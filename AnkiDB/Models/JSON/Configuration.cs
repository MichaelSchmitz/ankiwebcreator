﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace AnkiDB.Models.JSON
{
    /// <summary>
    /// Based on https://github.com/ankidroid/Anki-Android/wiki/Database-Structure#configuration-jsonobjects
    /// </summary>
    public class Configuration
    {
        [JsonPropertyName("curDeck")]
        /// <value>
        /// ID of last selected deck
        /// </value>
        public long CurrentDeckID { get; set; }

        [JsonPropertyName("activeDecks")]
        public List<long> ActiveDecks { get; set; }

        [JsonPropertyName("newSpread")]
        public OrderNewSpread OrderNewAndReview { get; set; }

        [JsonPropertyName("collapseTime")]
        /// <value>
        /// If no card for learning can be shown, but in TimeForLookahead is, it is shown now
        /// </value>
        public int TimeForLookahead { get; set; }

        [JsonPropertyName("timeLim")]
        public int TimeForShowingReviewedCards { get; set; }

        [JsonPropertyName("estTimes")]
        public bool ShowNextReviewTime { get; set; }

        [JsonPropertyName("dueCounts")]
        public bool ShowRemainingCardCount { get; set; }

        [JsonPropertyName("curModel")]
        public string CurrentModelID { get; set; }

        [JsonPropertyName("nextPos")]
        public int MaximumDueForNewCard { get; set; }

        [JsonPropertyName("sortType")]
        /// <value>
        /// value should be one of ActiveColumns, but not any of question, answer, template, deck, note or noteTags
        /// </value>
        public string SortType { get; set; }

        [JsonPropertyName("sortBackwards")]
        public bool SortBackwards { get; set; }

        [JsonPropertyName("addToCur")]
        public bool AddToCurrent { get; set; }

        [JsonPropertyName("newBury")]
        /// <value>
        /// Always set to true, unused?
        /// </value>
        public bool NewBury { get; set; }

        [JsonPropertyName("lastUnburied")]
        /// <value>
        /// date of the last time the scheduler was initialized or reset
        /// If not today, then buried notes must be unburied
        /// Not in JSON until scheduler is used once
        /// </value>
        public string LastUnburied { get; set; }


        [JsonPropertyName("activeCols")]
        /// <value>
        /// List of columns to show in browser, valid values: 
        ///   Value      | Browser column
        /// ------------------------------
        ///   question   | Question
        ///   answer     | Answer
        ///   template   | Card
        ///   deck       | Deck
        ///   noteFld    | Sort Field
        ///   noteCrt    | Created
        ///   noteMod    | Edited
        ///   cardMod    | Changed
        ///   cardDue    | Due
        ///   cardIvl    | Interval
        ///   cardEase   | Ease
        ///   cardReps   | Reviews
        ///   cardLapses | Lapses
        ///   noteTags   | Tags
        ///   note       | Note
        /// Defaults are: noteFld, template, cardDue and deck
        /// Not in JSON at creation, added when the browser is opened
        /// </value>
        public List<string> ActiveColumns { get; set; }

        public enum OrderNewSpread
        {
            NewCardsDistribute = 0, NewCardsLast = 1, NewCardsFirst = 2
        }
    }
}
