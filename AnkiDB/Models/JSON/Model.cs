﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace AnkiDB.Models.JSON
{
    /// <summary>
    /// Based on https://github.com/ankidroid/Anki-Android/wiki/Database-Structure#models-jsonobjects
    /// </summary>
    public class Model
    {
        [JsonPropertyName("css")]
        public string CSS { get; set; }

        [JsonPropertyName("did")]
        public long DeckID { get; set; }

        [JsonPropertyName("flds")]
        public List<Field> Fields { get; set; }

        [JsonPropertyName("id")]
        public long ModelID { get; set; }

        [JsonPropertyName("latexPost")]
        public string LatexPostExpression { get; set; }

        [JsonPropertyName("latexPre")]
        public string LatexPreExpression { get; set; }

        [JsonPropertyName("mod")]
        /// <value>
        /// in seconds
        /// </value>
        public long ModificationTime { get; set; }

        [JsonPropertyName("name")]
        public string ModelName { get; set; }

        [JsonPropertyName("req")]
        public List<object> RequiredFields { get; set; }

        [JsonPropertyName("sortf")]
        public int SortField { get; set; }

        [JsonPropertyName("tags")]
        public List<string> Tags { get; set; } = new List<string>();

        [JsonPropertyName("tmpls")]
        public List<Template> Templates { get; set; }

        [JsonPropertyName("type")]
        /// <value>
        /// Type of model
        /// 0 standard
        /// 1 cloze
        /// </value>
        public int Type { get; set; }

        [JsonPropertyName("usn")]
        /// <value>
        /// used to figure out diffs when syncing. 
        /// value of -1 indicates changes that need to be pushed to server.
        /// usn &lt; server usn indicates changes that need to be pulled from server.
        /// </value>
        public int UpdateSequenceNumber { get; set; }

        [JsonPropertyName("vers")]
        /// <value>
        /// Legacy version number, unused
        /// </value>
        public List<string> Version { get; set; } = new List<string>();

        public class Field
        {
            [JsonPropertyName("font")]
            public string Font { get; set; }

            [JsonPropertyName("media")]
            public List<string> Media { get; set; }

            [JsonPropertyName("name")]
            public string Name { get; set; }

            [JsonPropertyName("ord")]
            /// <value>
            /// Ordinal of the fields, 0 to num fields -1
            /// </value>
            public int Ordinal { get; set; }

            [JsonPropertyName("rtl")]
            public bool RightToLeft { get; set; }

            [JsonPropertyName("size")]
            public int FontSize { get; set; }

            [JsonPropertyName("sticky")]
            /// <value>
            /// Retain value that was last added when adding notes
            /// </value>
            public bool Sticky { get; set; }
        }

        public class Template
        {
            [JsonPropertyName("afmt")]
            public string AnswerFormat { get; set; }

            [JsonPropertyName("bafmt")]
            public string BrowserAnswerFormat { get; set; }

            [JsonPropertyName("bqfmt")]
            public string BrowserQuestionFormat { get; set; }

            [JsonPropertyName("did")]
            /// <value>
            /// Deck override
            /// </value>
            public string DeckId { get; set; } = null;

            [JsonPropertyName("name")]
            public string Name { get; set; }

            [JsonPropertyName("ord")]
            public int Ordinal { get; set; }

            [JsonPropertyName("qfmt")]
            public string QuestionFormat { get; set; }

        }

    }
}
