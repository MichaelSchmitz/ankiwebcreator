﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace AnkiDB.Models.JSON
{
    /// <summary>
    /// Based on https://github.com/ankidroid/Anki-Android/wiki/Database-Structure#dconf-jsonobjects
    /// </summary>
    public class DeckConfiguration
    {
        [JsonPropertyName("autoplay")]
        public bool Autoplay { get; set; }

        [JsonPropertyName("dyn")]
        public bool Dynamic { get; set; }

        [JsonPropertyName("id")]
        public long DeckID { get; set; }

        [JsonPropertyName("lapse")]
        public LapseConfig LapseConfiguration { get; set; }

        [JsonPropertyName("maxTaken")]
        /// <value>
        /// Timer is stopped after this amount (in s)
        /// </value>
        public int TimerMaximum { get; set; }
        
        [JsonPropertyName("mod")]
        public long LastModificationTime { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("new")]
        public NewCardConfig NewCardConfiguration { get; set; }

        [JsonPropertyName("replayq")]
        public bool ReplayAudioOnAnswer { get; set; }

        [JsonPropertyName("rev")]
        public ReviewConfig ReviewConfiguration { get; set; }

        [JsonPropertyName("timer")]
        /// <value>
        /// 0 show
        /// 1 dont show
        /// </value>
        public int ShowTimer { get; set; }

        [JsonPropertyName("usn")]
        /// <value>
        /// used to figure out diffs when syncing. 
        /// value of -1 indicates changes that need to be pushed to server.
        /// usn &lt; server usn indicates changes that need to be pulled from server.
        /// </value>
        public int UpdateSequenceNumber { get; set; }


        public class LapseConfig
        {

            [JsonPropertyName("delays")]
            public List<double> Delays { get; set; }

            [JsonPropertyName("leechAction")]
            public LapseLeechAction LeechAction { get; set; }

            [JsonPropertyName("leechFails")]
            /// <value>
            /// Number of lapses till leechAction is performed
            /// </value>
            public int LeechFails { get; set; }

            [JsonPropertyName("minInt")]
            /// <value>
            /// lower limit for new interval after leech
            /// </value>
            public int MinimumInterval { get; set; }

            [JsonPropertyName("mult")]
            /// <value>
            /// multiplier for interval when a card has lapsed
            /// </value>
            public double Multiplier { get; set; }

            /// <summary>
            /// Numbers according to the order in which the choices appear in aqt/dconf.ui"
            /// </summary>
            public enum LapseLeechAction
            {
                Suspend = 0, Mark = 1
            }
        }

        public class NewCardConfig
        {

            [JsonPropertyName("bury")]
            public bool Bury { get; set; }

            [JsonPropertyName("delays")]
            /// <value>
            /// successive delays between learning steps
            /// </value>
            public List<double> StepDelays { get; set; }

            [JsonPropertyName("initialFactor")]
            public int InitialEaseFactor { get; set; }

            [JsonPropertyName("ints")]
            /// <value>
            /// delay between learning steps (on button pressed)
            /// Good (Graduating), easy (and unused)
            /// </value>
            public List<int> DelayIntervals { get; set; }

            [JsonPropertyName("order")]
            public NewShowOrder ShowOrder { get; set; }

            [JsonPropertyName("perDay")]
            public int MaximumPerDay { get; set; }

            [JsonPropertyName("separate")]
            /// <value>
            /// unused
            /// </value>
            public bool Separate { get; set; }

            public enum NewShowOrder
            {
                Random = 0, Due = 1
            }

        }

        public class ReviewConfig
        {

            [JsonPropertyName("bury")]
            public bool Bury { get; set; }

            [JsonPropertyName("ease4")]
            public double AddOnEasyPress { get; set; }

            [JsonPropertyName("fuzz")]
            /// <value>
            /// New interval is generated between -RandomIntervalBorder and +RandomIntervalBorder
            /// </value>
            public double RandomIntervalBorder { get; set; }

            [JsonPropertyName("ivlFct")]
            public double IntervalMultiplicationFactor { get; set; }

            [JsonPropertyName("maxIvl")]
            public int MaximumReviewInterval { get; set; }

            [JsonPropertyName("minSpace")]
            /// <value>
            /// unused
            /// </value>
            public int MinimumSpace { get; set; }

            [JsonPropertyName("perDay")]
            public int MaximumPerDay { get; set; }
        }
    }
}
