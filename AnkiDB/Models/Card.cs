﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnkiDB.Models
{
    [Table("cards")]
    [Index(nameof(NoteID))]
    [Index(nameof(UpdateSequenceNumber))]
    [Index(nameof(DeckID), nameof(Queue), nameof(Due))]
    /// <summary>
    /// Cards table based on https://github.com/ankidroid/Anki-Android/wiki/Database-Structure#database-schema
    /// </summary>
    public class Card : IUsnMod
    {
        [Column("id", Order = 0)]
        /// <value>
        /// the epoch milliseconds of when the entity was created
        /// </value>
        public long ID { get; set; }

        [Column("nid", Order = 1)]
        [Required]
        public long NoteID { get; set; }

        [Column("did", Order = 2)]
        [Required]
        public long DeckID { get; set; }

        [Column("ord", Order = 3)]
        [Required]
        /// <value>
        /// identifies which of the card templates or cloze deletions it corresponds to 
        /// for card templates, valid values are from 0 to num templates - 1
        /// for cloze deletions, valid values are from 0 to max cloze index - 1 (they're 0 indexed despite the first being called `c1`)
        /// </value>
        public int Ordinal { get; set; }

        [Column("mod", Order = 4)]
        /// <value>
        /// Timestamp in epoch seconds
        /// </value>
        public long ModificationTime { get; set; }

        [Column("usn", Order = 5)]
        /// <value>
        /// used to figure out diffs when syncing. 
        /// value of -1 indicates changes that need to be pushed to server.
        /// usn &lt; server usn indicates changes that need to be pulled from server.
        /// </value>
        public int UpdateSequenceNumber { get; set; }

        [Column("type", Order = 6)]
        [Required]
        public CardType Type { get; set; }

        [Column("queue", Order = 7)]
        [Required]
        public QueueType Queue { get; set; }

        [Column("due", Order = 8)]
        [Required]
        /// <value>
        /// used differently for different card types: 
        ///      new: note id or random int
        ///      due: integer day, relative to the collection's creation time
        ///      learning: integer timestamp in second
        /// </value>
        public long Due { get; set; }

        [Column("ivl", Order = 9)]
        [Required]
        /// <value>
        /// Interval (used in SRS algorithm). Negative = seconds, positive = days
        /// </value>
        public int Interval { get; set; }

        [Column("factor", Order = 10)]
        [Required]
        /// <value>
        /// ease factor of the card in permille (parts per thousand).
        /// If the ease factor is 2500, the card’s interval will be multiplied by 2.5 the next time you press Good.
        /// </value>
        public int EaseFactor { get; set; }

        [Column("reps", Order = 11)]
        [Required]
        /// <value>
        /// Number of card reviews
        /// </value>
        public int Repetitions { get; set; }

        [Column("lapses", Order = 12)]
        [Required]
        /// <value>
        /// number of times the card went from a "was answered correctly" to "was answered incorrectly" state
        /// </value>
        public int Relapses { get; set; }

        [Column("left", Order = 13)]
        [Required]
        /// <value>
        /// Number of reps left with form a*1000+b:
        ///    b being the number of reps left till graduation
        ///    a being the number of reps left today
        /// </value>
        public int Left { get; set; }

        [Column("odue", Order = 14)]
        [Required]
        /// <value>
        /// In filtered decks, it's the original due date that the card had before moving to filtered.
        /// If the card lapsed in scheduler1, then it's the value before the lapse. (This is used when switching to scheduler 2. At this time, cards in learning becomes due again, with their previous due date)
        /// In any other case it's 0.
        /// </value>
        public int OriginalDue { get; set; }

        [Column("odid", Order = 15)]
        [Required]
        /// <value>
        /// used when the card is currently in filtered deck
        /// </value>
        public int OriginalDeckId { get; set; }

        [Column("flags", Order = 16)]
        [Required]
        public CardFlags Flags { get; set; }

        [Column("data", Order = 17)]
        [Required]
        /// <value>
        /// unused
        /// </value>
        public string Data { get; set; }

        public enum CardType
        {
            New = 0, Learning = 1, Review = 2, Relearning = 3
        }

        public enum QueueType
        {
            UserBuried = -3, SchedulerBuried = -2, Buried = -2, Suspended = -1, New = 0, Learning = 1, Review = 2, InLearning = 3, Preview = 4
        }

        public enum CardFlags
        {
            NoFlag = 0, Red = 1, Orange = 2, Green = 3, Blue = 4, Custom1 = 5, Custom2 = 6, Custom3 = 7
        }
    }
}
