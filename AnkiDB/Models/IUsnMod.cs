﻿namespace AnkiDB.Models
{
    interface IUsnMod
    {
        public long ID { get; set; }
        public int UpdateSequenceNumber { get; set; }
        public long ModificationTime { get; set; }
    }
}
