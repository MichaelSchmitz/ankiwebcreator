﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnkiDB.Models
{
    [Table("col")]
    /// <summary>
    /// Collection table based on https://github.com/ankidroid/Anki-Android/wiki/Database-Structure#database-schema
    /// </summary>
    public class CollectionInformation : IUsnMod
    {
        [Column("id", Order = 0)]
        [Required]
        /// <value>
        /// arbitrary number, only one row ever
        /// </value>
        public long ID { get; set; } = 0;

        [Column("crt", Order = 1)]
        [Required]
        /// <value>
        /// Timestamp in seconds, it's correct up to the day. For V1 scheduler, the hour corresponds to starting a new day. By default, new day is 4
        /// </value>
        public long CreationTimestamp { get; set; }

        [Column("mod", Order = 2)]
        /// <value>
        /// Timestamp in epoch seconds
        /// </value>
        public long ModificationTime { get; set; }

        [Column("scm", Order = 3)]
        [Required]
        /// <value>
        /// time when "schema" was modified. If different on the server then on the client a full-sync is required
        /// </value>
        public long SchemaModificationTime { get; set; }

        [Column("ver", Order = 4)]
        [Required]
        public int Version { get; set; }

        [Column("dty", Order = 5)]
        [Required]
        /// <value>
        /// unused
        /// </value>
        public int Dirty { get; set; } = 0;

        [Column("usn", Order = 6)]
        /// <value>
        /// used to figure out diffs when syncing. 
        /// value of -1 indicates changes that need to be pushed to server.
        /// usn &lt; server usn indicates changes that need to be pulled from server.
        /// </value>
        public int UpdateSequenceNumber { get; set; }

        [Column("ls", Order = 7)]
        [Required]
        public int LastSyncTime { get; set; }

        [Column("conf", Order = 8)]
        [Required]
        /// <value>
        /// Mapped to JSON
        /// </value>
        public JSON.Configuration Configuration { get; set; }

        [Column("models", Order = 9)]
        [Required]
        /// <value>
        /// Mapped to JSON
        /// </value>
        public Dictionary<long, JSON.Model> Models { get; set; }

        [Column("decks", Order = 10)]
        [Required]
        /// <value>
        /// Mapped to JSON
        /// </value>
        public Dictionary<long, JSON.Deck> Decks { get; set; }

        [Column("dconf", Order = 11)]
        [Required]
        /// <value>
        /// Mapped to JSON
        /// </value>
        public Dictionary<long, JSON.DeckConfiguration> DeckConfigurations { get; set; }

        [Column("tags", Order = 12)]
        [Required]
        /// <value>
        /// a cache of tags used in the collection (This list is displayed in the browser. Potentially at other place)
        /// </value>
        public string Tags { get; set; }


    }
}
