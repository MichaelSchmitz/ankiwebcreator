﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnkiDB.Models
{
    [Table("revlog")]
    [Index(nameof(CardID))]
    [Index(nameof(UpdateSequenceNumber))]
    /// <summary>
    /// ReviewHistory table based on https://github.com/ankidroid/Anki-Android/wiki/Database-Structure#database-schema
    /// </summary>
    public class Review
    {
        [Column("id", Order = 0)]
        [Required]
        /// <value>
        /// the epoch milliseconds of when the entity was created
        /// </value>
        public long ID { get; set; }

        [Column("cid", Order = 1)]
        [Required]
        public long CardID { get; set; }

        [Column("usn", Order = 2)]
        [Required]
        /// <value>
        /// used to figure out diffs when syncing. 
        /// value of -1 indicates changes that need to be pushed to server.
        /// usn &lt; server usn indicates changes that need to be pulled from server.
        /// </value>
        public int UpdateSequenceNumber { get; set; }

        [Column("ease", Order = 3)]
        [Required]
        /// <value>
        /// which button was pushed to score recall
        /// </value>
        public ReviewEase Ease { get; set; }

        [Column("ivl", Order = 4)]
        [Required]
        /// <value>
        /// Interval (like in the Cards table)
        /// </value>
        public int Interval { get; set; }

        [Column("lastIvl", Order = 5)]
        [Required]
        /// <value>
        /// last interval (i.e. the last value of ivl.)
        /// Note that this value is not necessarily equal to the actual interval between this review and the preceding review
        /// </value>
        public int LastInterval { get; set; }

        [Column("factor", Order = 6)]
        [Required]
        public int Factor { get; set; }

        [Column("time", Order = 7)]
        [Required]
        /// <value>
        /// Time the review took in ms (max 60.000)
        /// </value>
        public int ReviewTime { get; set; }

        [Column("type", Order = 8)]
        [Required]
        public ReviewType Type { get; set; }

        public enum ReviewEase
        {
            ReviewWrong = 1, ReviewHard = 2, ReviewOK = 3, ReviewEasy = 4,
            LearnWrong = 1, LearnOK = 2, LearnEasy = 3

        }

        public enum ReviewType
        {
            Learn = 0, Review = 1, Relearn = 2, Cram = 3
        }
    }
}
