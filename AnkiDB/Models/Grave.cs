﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnkiDB.Models
{
    [Table("graves")]
    [Keyless]
    /// <summary>
    /// Graves table based on https://github.com/ankidroid/Anki-Android/wiki/Database-Structure#database-schema
    /// </summary>
    public class Grave
    {
        [Column("oid", Order = 1)]
        [Required]
        public long OriginalID { get; set; }

        [Column("usn", Order = 0)]
        [Required]
        public int UpdateSequenceNumber { get; set; } = -1;

        [Column("type", Order = 2)]
        [Required]
        /// <value>
        /// 0 for card
        /// 1 for note
        /// 2 for deck
        /// </value>
        public EntryType Type { get; set; }

        public enum EntryType
        {
            Card = 0, Note = 1, Deck = 2
        }
    }
}
