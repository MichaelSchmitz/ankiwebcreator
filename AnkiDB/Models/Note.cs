﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Numerics;
using System.Security.Cryptography;
using System.Text;

namespace AnkiDB.Models
{
    [Table("notes")]
    [Index(nameof(FieldChecksum))]
    [Index(nameof(UpdateSequenceNumber))]
    /// <summary>
    /// Notes table based on https://github.com/ankidroid/Anki-Android/wiki/Database-Structure#database-schema
    /// </summary>
    public class Note : IUsnMod
    {
        [Column("id", Order = 0)]
        /// <value>
        /// the epoch milliseconds of when the entity was created
        /// </value>
        public long ID { get; set; }

        [Column("guid", Order = 1)]
        [Required]
        /// <value>
        /// Globally unique ID, used for syncing
        /// </value>
        public string GUID { get; set; }

        [Column("mid", Order = 2)]
        [Required]
        public long ModelID { get; set; }

        [Column("mod", Order = 3)]
        /// <value>
        /// Timestamp in epoch seconds
        /// </value>
        public long ModificationTime { get; set; }

        [Column("usn", Order = 4)]
        /// <value>
        /// used to figure out diffs when syncing. 
        /// value of -1 indicates changes that need to be pushed to server.
        /// usn &lt; server usn indicates changes that need to be pulled from server.
        /// </value>
        public int UpdateSequenceNumber { get; set; }

        [Column("tags", Order = 5)]
        [Required]
        /// <value>
        /// space-separated string of tags, includes space at the beginning and end, for LIKE "% tag %" queries
        /// </value>
        public string Tags { get; set; }

        [Column("flds", Order = 6)]
        [Required]
        /// <value>
        /// separated by 0x1f (31) character
        /// </value>
        public string FieldValues { get; set; }

        [Column("sfld", TypeName = "INTEGER", Order = 7)]
        [Required]
        /// <value>
        /// used for quick sorting and duplicate check
        /// sort field is an integer so that when users are sorting on a field that contains only numbers, they are sorted in numeric instead of lexical order
        /// Text is stored in this integer field
        /// </value>
        public string SortField { get; set; }

        [Column("csum", Order = 8)]
        [Required]
        /// <value>
        /// Checksum used for duplicate check
        /// integer representation of first 8 digits of sha1 hash of the first field
        /// </value>
        public long FieldChecksum { get; set; }

        [Column("flags", Order = 9)]
        [Required]
        /// <value>
        /// unused
        /// </value>
        public int Flags { get; set; }

        [Column("data", Order = 10)]
        [Required]
        /// <value>
        /// unused
        /// </value>
        public string Data { get; set; }

        public void SetFields(List<string> fields)
        {
            FieldValues = ToSeparatedString(fields);
            SortField = fields[0];
            var fieldBytes = Encoding.UTF8.GetBytes(fields[0]);
            FieldChecksum = CalculateChecksum(fieldBytes);
        }

        private static long CalculateChecksum(byte[] input)
        {
            using SHA1Managed sha1 = new();
            var hash = sha1.ComputeHash(input);
            return long.Parse(new BigInteger(hash).ToString()[..8]);
        }

        private static string ToSeparatedString(List<string> fields)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendJoin('\x1f', fields);
            return stringBuilder.ToString();
        }
    }
}
