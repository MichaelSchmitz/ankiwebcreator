﻿using AnkiDB.Database;
using AnkiDB.Models;
using AnkiDB.Models.JSON;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using static AnkiDB.Models.Card;
using static AnkiDB.Models.Review;

namespace AnkiDB.Services
{
    public class AnkiService
    {
        private readonly DatabaseContextFactory factory;
        private readonly Dictionary<string, DatabaseContext> dbContexts;
        private readonly Dictionary<string, Dictionary<string, string>> mediaMappings;
        public event EventHandler<EventArgs> DeckRegistered;


        public AnkiService(DatabaseContextFactory databaseContextFactory)
        {
            factory = databaseContextFactory;
            dbContexts = new();
            mediaMappings = new();
            Directory.CreateDirectory("decks");
            foreach (var directory in Directory.GetDirectories("decks"))
            {
                var dbFile = Path.Combine(directory, "collection.anki2");
                var name = directory[(directory.LastIndexOf('/') + 1)..];
                if (File.Exists(dbFile)) {
                    var dbContext = factory.CreateDbContext(new string[] { dbFile });
                    dbContexts.Add(name, dbContext);
                }
                var mediaFile = Path.Combine(directory, "media");
                if (File.Exists(mediaFile))
                    ReadMediaFile(name, mediaFile);
            }
        }

        public IEnumerable<string> GetRegisteredDecks()
        {
            return dbContexts.Keys.AsEnumerable();
        }

        public async Task Init(string name, string filePath = null)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("Name is required");
            if (dbContexts.ContainsKey(name))
                return;
            var containingDirectory = Path.Combine("decks", name);
            Directory.CreateDirectory(containingDirectory);
            if (filePath != null)
                ExtractApkgFile(name, filePath, containingDirectory);
            if (!mediaMappings.ContainsKey(name))
                mediaMappings.Add(name, new());
            await WriteMediaFile(name);
            var dbFile = Path.Combine(containingDirectory, "collection.anki2");
            var dbContext = factory.CreateDbContext(new string[]{ dbFile});
            dbContexts.Add(name, dbContext);
            DeckRegistered.Invoke(null, null);
        }

        #region Notes

        /// <summary>
        /// Get all notes for given deck from database context
        /// </summary>
        /// <param name="deckName">Deck to query from</param>
        /// <returns>List of notes</returns>
        /// <exception cref="InvalidOperationException">Thrown, when Init was not called before call to model</exception>
        public async Task<List<Note>> GetNotes(string deckName)
        {
            CheckInitialized(deckName);
            return await dbContexts[deckName].Notes.ToListAsync();
        }

        /// <summary>
        /// Add given note to the database context for given deck
        /// </summary>
        /// <param name="deckName">Deck to add to</param>
        /// <param name="note">Note to add</param>
        /// <exception cref="InvalidOperationException">Thrown, when Init was not called before call to model</exception>
        public void AddNote(string deckName, Note note)
        {
            CheckInitialized(deckName);
            if (note == null)
                return;
            dbContexts[deckName].Notes.Add(note);
        }

        /// <summary>
        /// Create a note with given parameters and add it to the database context for given deck
        /// </summary>
        /// <param name="deckName">Deck to add to</param>
        /// <param name="modelID">ModelID for note</param>
        /// <param name="fields">List of fields to set, number of fields has to be equal to the number in the given model</param>
        /// <param name="tags">List of tags the note should have</param>
        /// <exception cref="InvalidOperationException">Thrown, when Init was not called before call to model</exception>
        /// <exception cref="ArgumentException">Thrown, if the model is not found for given deck</exception>
        /// <exception cref="ArgumentException">Thrown, when counts do not match</exception>
        public void AddNote(string deckName, long modelID, List<string> fields, List<string> tags = default)
        {
            CheckInitialized(deckName);
            CheckModelID(deckName, modelID);
            CheckFieldCount(deckName, modelID, fields.Count);
            var note = new Note()
            {
                ID = DateTimeOffset.Now.ToUnixTimeMilliseconds(),
                GUID = new Guid().ToString(),
                ModelID = modelID,
                Tags = string.Join(' ', tags),
                UpdateSequenceNumber = 0,

            };
            note.SetFields(fields);
            AddNote(deckName, note);
        }


        /// <summary>
        /// Mark given card for deletion in database context for given deck
        /// </summary>
        /// <param name="deckName">Deck to delete from</param>
        /// <param name="note">Card to delete</param>
        /// <exception cref="InvalidOperationException">Thrown, when Init was not called before call to model</exception>
        public void DeleteNote(string deckName, Note note)
        {
            CheckInitialized(deckName);
            if (note == null)
                return;
            dbContexts[deckName].Notes.Remove(note);
        }

        /// <summary>
        /// Update given note in database context for given deck
        /// </summary>
        /// <param name="deckName">Deck to update in</param>
        /// <param name="note">Card to update</param>
        /// <exception cref="InvalidOperationException">Thrown, when Init was not called before call to model</exception>
        public void UpdateNote(string deckName, Note note)
        {
            CheckInitialized(deckName);
            dbContexts[deckName].Notes.Update(note);
        }        
        #endregion

        #region Cards

        /// <summary>
        /// Get all cards for given deck
        /// </summary>
        /// <param name="deckName">Deck to query</param>
        /// <returns>List of cards</returns>
        /// <exception cref="InvalidOperationException">Thrown, when Init was not called before call to model</exception>
        public async Task<List<Card>> GetCards(string deckName)
        {
            CheckInitialized(deckName);
            return await dbContexts[deckName].Cards.ToListAsync();
        }

        /// <summary>
        /// Add given card to the database context for given deck
        /// </summary>
        /// <param name="deckName">Deck to add to</param>
        /// <param name="card">Card to add</param>
        /// <exception cref="InvalidOperationException">Thrown, when Init was not called before call to model</exception>
        public void AddCard(string deckName, Card card)
        {
            CheckInitialized(deckName);
            if (card == null)
                return;
            dbContexts[deckName].Cards.Add(card);
        }

        /// <summary>
        /// Create card with given parameters and add it to the database context of given deck
        /// </summary>
        /// <param name="deckName">Deck to add to</param>
        /// <param name="noteID">Note the card is based on</param>
        /// <param name="interval">Interval for SRS algorithm, negative: seconds, positive: days</param>
        /// <param name="easeFactor">ease factor for interval multiplications in permille</param>
        /// <param name="repetitions">Number of performed repetitions</param>
        /// <param name="relapses">Number of false answered after correctly answered</param>
        /// <param name="left">Number of reps left with form (repsToday)*1000+(repsTillGraduation)</param>
        /// <param name="flags">Red 1, Orange 2, Green 3, Blue 4, no flag: 0 (Range 0-7)</param>
        /// <exception cref="InvalidOperationException">Thrown, when Init was not called before call to model</exception>
        public void AddCard(string deckName, long noteID, int interval = default, int easeFactor = default, int repetitions = default, int relapses = default, int left = default, CardFlags flags = default)
        {
            CheckInitialized(deckName);
            CheckNoteID(deckName, noteID);
            var deckID = GetDeckID(deckName, dbContexts[deckName].CollectionInformation.First().Decks).GetValueOrDefault(default);
            var card = new Card()
            {
                ID = DateTimeOffset.Now.ToUnixTimeMilliseconds(),
                NoteID = noteID,
                DeckID = deckID,
                Type = CardType.New,
                Queue = QueueType.New,
                Due = noteID, 
                Interval = interval,
                EaseFactor = easeFactor,
                Repetitions = repetitions,
                Relapses = relapses,
                Left = left,
                Flags = flags,
                UpdateSequenceNumber = 0,
                Data = ""

            };
            AddCard(deckName, card);
        }

        /// <summary>
        /// Mark the given card for deletion on database context for given deck
        /// </summary>
        /// <param name="deckName">Deck to delete from</param>
        /// <param name="card">Card to delete</param>
        /// <exception cref="InvalidOperationException">Thrown, when Init was not called before call to model</exception>
        public void DeleteCard(string deckName, Card card)
        {
            CheckInitialized(deckName);
            if (card == null)
                return;
            dbContexts[deckName].Cards.Remove(card);
        }


        /// <summary>
        /// Update given card in database context for given deck
        /// </summary>
        /// <param name="deckName">Deck to update in</param>
        /// <param name="card">Card to update</param>
        /// <exception cref="InvalidOperationException">Thrown, when Init was not called before call to model</exception>
        public void UpdateCard(string deckName, Card card)
        {
            CheckInitialized(deckName);
            dbContexts[deckName].Cards.Update(card);
        }
        #endregion

        #region CollectionInformation

        /// <summary>
        /// Get collection information (configuration) for given deck
        /// </summary>
        /// <param name="deckName"></param>
        /// <returns>collection information (there should always be only one in the database)</returns>
        /// <exception cref="InvalidOperationException">Thrown, when Init was not called before call to model</exception>
        public async Task<CollectionInformation> GetCollectionInformation(string deckName)
        {
            CheckInitialized(deckName);
            return await dbContexts[deckName].CollectionInformation.FirstAsync();
        }

        /// <summary>
        /// Create collection information with given params and add it to the database context for given deck
        /// 
        /// Warning: Only one entry can be present at a time, fails if one entry is already present
        /// </summary>
        /// <param name="deckName">Deck to add to</param>
        /// <param name="configuration">Configuration of collection</param>
        /// <param name="models">Dictionary with modelID as key</param>
        /// <param name="decks">Dictionary with deckID as key</param>
        /// <param name="deckConfigurations">Dictionary with deckID as key</param>
        /// <param name="tags">Cache of tags</param>
        /// <exception cref="InvalidOperationException">Thrown, when Init was not called before call to model</exception>
        /// <exception cref="InvalidOperationException">Thrown, when already one entry is present on database context, use the update method instead</exception>
        public void AddCollectionInformation(string deckName, Configuration configuration, Dictionary<long, Model> models, Dictionary<long, Deck> decks, Dictionary<long, DeckConfiguration> deckConfigurations, string tags = "{}")
        {
            CheckInitialized(deckName);
            if (models == null || decks == null || deckConfigurations == null)
                throw new ArgumentNullException("Models, decks and DeckConfigurations cannot be null");
            if (models.Count == 0 || decks.Count == 0 || deckConfigurations.Count == 0)
                throw new ArgumentException("Models, decks and DeckConfigurations each have to have at least one entry");
            var collectionInformation = new CollectionInformation()
            {
                ID = 1,
                CreationTimestamp = DateTimeOffset.Now.ToUnixTimeSeconds(),
                SchemaModificationTime = DateTimeOffset.Now.ToUnixTimeMilliseconds(),
                Version = 11,
                LastSyncTime = -1,
                Configuration = configuration,
                Models = models,
                Decks = decks,
                DeckConfigurations = deckConfigurations,
                Tags = tags,
                UpdateSequenceNumber = 0,
            };
            AddCollectionInformation(deckName, collectionInformation);
        }

        /// <summary>
        /// Add given collection information to the database context for given deck
        /// 
        /// Warning: Only one entry can be present at a time, fails if one entry is already present
        /// </summary>
        /// <param name="deckName">Deck to add to</param>
        /// <param name="collectionInformation">Collection information to add</param>
        /// <exception cref="InvalidOperationException">Thrown, when Init was not called before call to model</exception>
        /// <exception cref="InvalidOperationException">Thrown, when already one entry is present on database context, use the update method instead</exception>
        public void AddCollectionInformation(string deckName, CollectionInformation collectionInformation)
        {
            CheckInitialized(deckName);
            if (collectionInformation == null)
                return;
            if (dbContexts[deckName].CollectionInformation.Count() != 0)
                throw new InvalidOperationException("Only one entry can be present at a time");
            dbContexts[deckName].CollectionInformation.Add(collectionInformation);
        }

        /// <summary>
        /// Delete collection information from database context for given deck
        /// </summary>
        /// <param name="deckName">Deck to delete from</param>
        /// <exception cref="InvalidOperationException">Thrown, when Init was not called before call to model</exception>
        public async Task DeleteCollectionInformation(string deckName)
        {
            CheckInitialized(deckName);
            dbContexts[deckName].CollectionInformation.Remove(await GetCollectionInformation(deckName));
        }

        /// <summary>
        /// Update given collection information on the database context for given deck
        /// </summary>
        /// <param name="deckName">Deck to update on</param>
        /// <param name="collectionInformation">Collection information to update</param>
        /// <exception cref="InvalidOperationException">Thrown, when Init was not called before call to model</exception>
        public void UpdateCollectionInformation(string deckName, CollectionInformation collectionInformation)
        {
            CheckInitialized(deckName);
            if (collectionInformation == null)
                return;
            dbContexts[deckName].CollectionInformation.Update(collectionInformation);
        }
        #endregion

        #region Graves
        /// <summary>
        /// Get all graves for given deck
        /// </summary>
        /// <param name="deckName"></param>
        /// <returns>List of graves</returns>
        /// <exception cref="InvalidOperationException">Thrown, when Init was not called before call to model</exception>
        public async Task<List<Grave>> GetGraves(string deckName)
        {
            CheckInitialized(deckName);
            return await dbContexts[deckName].Graves.ToListAsync();
        }

        /// <summary>
        /// Create grave with given params and add it to the database context for given deck
        /// </summary>
        /// <param name="deckName">Deck to add to</param>
        /// <param name="ID">OriginalID of grave</param>
        /// <param name="type">Original type of entry</param>
        /// <exception cref="InvalidOperationException">Thrown, when Init was not called before call to model</exception>
        public void AddGrave(string deckName, long ID, Grave.EntryType type) {
            var grave = new Grave()
            { 
                OriginalID = ID,
                Type = type,
                UpdateSequenceNumber = -1
            };
            AddGrave(deckName, grave);
        }

        /// <summary>
        /// Add given grave to the database context for given deck
        /// </summary>
        /// <param name="deckName">Deck to add to</param>
        /// <param name="grave">Grave to add</param>
        /// <exception cref="InvalidOperationException">Thrown, when Init was not called before call to model</exception>
        public void AddGrave(string deckName, Grave grave)
        {
            CheckInitialized(deckName);
            if (grave == null)
                return;
            dbContexts[deckName].Graves.Add(grave);
        }

        /// <summary>
        /// Delete given grave from the database context for given deck
        /// </summary>
        /// <param name="deckName">Deck to delete from</param>
        /// <param name="grave">Grave to delete</param>
        /// <exception cref="InvalidOperationException">Thrown, when Init was not called before call to model</exception>
        public void DeleteGrave(string deckName, Grave grave)
        {
            CheckInitialized(deckName);
            if (grave == null)
                return;
            dbContexts[deckName].Graves.Remove(grave);
        }

        /// <summary>
        /// Update given grave on the database context for given deck
        /// </summary>
        /// <param name="deckName">Deck to update on</param>
        /// <param name="grave">Grave to update</param>
        /// <exception cref="InvalidOperationException">Thrown, when Init was not called before call to model</exception>
        public void UpdateGrave(string deckName, Grave grave)
        {
            CheckInitialized(deckName);
            if (grave == null)
                return;
            dbContexts[deckName].Graves.Update(grave);
        }
        #endregion

        #region ReviewHistory
        /// <summary>
        /// Get all reviews for given deck
        /// </summary>
        /// <param name="deckName"></param>
        /// <returns>List of reviews</returns>
        /// <exception cref="InvalidOperationException">Thrown, when Init was not called before call to model</exception>
        public async Task<List<Review>> GetReviews(string deckName)
        {
            CheckInitialized(deckName);
            return await dbContexts[deckName].ReviewHistory.ToListAsync();
        }

        /// <summary>
        /// Create a review with given params and add it to the database context for given deck
        /// </summary>
        /// <param name="deckName">Deck to add to</param>
        /// <param name="cardID">ID of card reviewed</param>
        /// <param name="ease">Userbutton press, how easy the card was</param>
        /// <param name="interval">Interval for next review</param>
        /// <param name="lastInterval">Last value of interval (if card was reviewed beforehand)</param>
        /// <param name="factor">Intervalfactor</param>
        /// <param name="reviewTime">Time taken to perform review, in ms, max: 60.000</param>
        /// <param name="type">Which type of review this entry represents</param>
        /// <exception cref="InvalidOperationException">Thrown, when Init was not called before call to model</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown, when reviewTime is not in range 0 - 60000</exception>
        /// <exception cref="ArgumentException">Thrown, if the card is not found for given deck</exception>
        public void AddReview(string deckName, long cardID, ReviewEase ease, int interval, int lastInterval, int factor, int reviewTime, ReviewType type)
        {
            CheckInitialized(deckName);
            if (0 >= reviewTime && reviewTime > 60000)
                throw new ArgumentOutOfRangeException("Review time can only be in range 0 - 60.000ms");
            CheckCardID(deckName, cardID);
            var review = new Review()
            {
                ID = DateTimeOffset.Now.ToUnixTimeMilliseconds(),
                CardID = cardID,
                UpdateSequenceNumber = -1,
                Ease = ease,
                Interval = interval,
                LastInterval = lastInterval,
                Factor = factor,
                ReviewTime = reviewTime,
                Type = type,
            };
            AddReview(deckName, review);
        }

        /// <summary>
        /// Add given review to the database context for given deck
        /// </summary>
        /// <param name="deckName">Deck to add to</param>
        /// <param name="review">Review to add</param>
        /// <exception cref="InvalidOperationException">Thrown, when Init was not called before call to model</exception>
        public void AddReview(string deckName, Review review)
        {
            CheckInitialized(deckName);
            if (review == null)
                return;
            dbContexts[deckName].ReviewHistory.Add(review);
        }

        /// <summary>
        /// Delete given review from the database context for given deck
        /// </summary>
        /// <param name="deckName">Deck to delete from</param>
        /// <param name="review">Review to delete</param>
        /// <exception cref="InvalidOperationException">Thrown, when Init was not called before call to model</exception>
        public void DeleteReview(string deckName, Review review)
        {
            CheckInitialized(deckName);
            if (review == null)
                return;
            dbContexts[deckName].ReviewHistory.Remove(review);
        }

        /// <summary>
        /// Update given review on the database context for given deck
        /// </summary>
        /// <param name="deckName">Deck to update on</param>
        /// <param name="review">Review to update</param>
        /// <exception cref="InvalidOperationException">Thrown, when Init was not called before call to model</exception>
        public void UpdateReview(string deckName, Review review)
        {
            CheckInitialized(deckName);
            if (review == null)
                return;
            dbContexts[deckName].ReviewHistory.Update(review);
        }
        #endregion

        public async Task Save(string name)
        {
            CheckInitialized(name);
            await dbContexts[name].SaveChangesAsync();
        }

        #region APKG
        public void CreateOrOverwriteApkgFile(string name, string targetPath = "")
        {
            CheckInitialized(name);
            var file = Path.Combine(targetPath, $"{name}.apkg");
            if (targetPath != "")
                Directory.CreateDirectory(targetPath);
            if (File.Exists(file))
                File.Delete(file);
            ZipFile.CreateFromDirectory(Path.Combine("decks", name), file);
        }

        public void CreateApkgFile(string name, string targetPath = "")
        {
            CheckInitialized(name);
            if (targetPath != "")
                Directory.CreateDirectory(targetPath);
            ZipFile.CreateFromDirectory(Path.Combine("decks", name), Path.Combine(targetPath, $"{name}.apkg"));
        }


        private void ExtractApkgFile(string name, string filePath, string targetPath)
        {
            if (!filePath.EndsWith(".apkg"))
                throw new ArgumentException("Not an apkg file");
            if (!File.Exists(filePath))
                throw new ArgumentException("File not found");
            if (Directory.Exists(targetPath))
            {
                Directory.Delete(targetPath, true);
                Directory.CreateDirectory(targetPath);
            }

            ZipFile.ExtractToDirectory(filePath, targetPath);
            ReadMediaFile(name, Path.Combine(targetPath, "media"));
        }
        #endregion


        #region Media
        public async Task<bool> AddMediaFile(string name, string basePath, string fileName)
        {
            CheckInitializedMedia(name);
            var mapping = mediaMappings[name];
            if (mapping.ContainsValue(fileName))
                return false;
            var key = $"{mapping.Count}";
            mapping.Add(key, fileName);
            File.Copy(Path.Combine(basePath, fileName), Path.Combine("decks", name, key));
            await WriteMediaFile(name);
            return true;
        }

        public async Task RemoveMediaFile(string name, string basePath, string fileName)
        {
            CheckInitializedMedia(name);
            var mapping = mediaMappings[name];
            var key = mapping.Where(keyValue => keyValue.Value == fileName).Select(keyValue => keyValue.Key).SingleOrDefault(null);
            if (key == null)
                return;
            File.Delete(Path.Combine("decks", key));
            mapping.Remove(key);
            await WriteMediaFile (name);
        }

        private void ReadMediaFile(string name, string filePath)
        {
            var json = File.ReadAllText(filePath);
            var parsed = JsonSerializer.Deserialize<Dictionary<string, string>>(json);
            mediaMappings.Add(name, parsed);
        }

        private async Task WriteMediaFile(string name)
        {
            using var fileStream = File.Create(Path.Combine("decks", name, "media"));
            await JsonSerializer.SerializeAsync(fileStream, mediaMappings[name]);
            await fileStream.DisposeAsync();
        }
        #endregion

        #region Checks
        /// <summary>
        /// Checks if a database context exists for the given deck
        /// </summary>
        /// <param name="deckName">DeckName to check</param>
        /// <exception cref="InvalidOperationException">Thrown, when Init was not called before call to model</exception>
        private void CheckInitialized(string deckName)
        {
            if (!dbContexts.ContainsKey(deckName))
                throw new InvalidOperationException("Not yet initialized, call Init(name, [filePath]) first");
        }

        /// <summary>
        /// Checks if a media mappings dictionary exists for the given deck
        /// </summary>
        /// <param name="deckName">DeckName to check</param>
        /// <exception cref="InvalidOperationException">Thrown, when Init was not called before call to model</exception>
        private void CheckInitializedMedia(string deckName)
        {
            if (!mediaMappings.ContainsKey(deckName))
                throw new InvalidOperationException("Not yet initialized, call Init(name, [filePath]) first");
        }

        /// <summary>
        /// Checks if the given ModelID exists for the given deck
        /// </summary>
        /// <param name="deckName">DeckName to check against</param>
        /// <param name="modelID">ModelID to check</param>
        /// <exception cref="ArgumentException">Thrown, if the model is not found for given deck</exception>
        private void CheckModelID(string deckName, long modelID)
        {
            try
            {
                dbContexts[deckName].CollectionInformation.Single(entry => entry.Models.ContainsKey(modelID));
            }
            catch (InvalidOperationException)
            {
                throw new ArgumentException("Invalid modelID");
            }
        }

        /// <summary>
        /// Checks if the given NoteID exists for the given deck
        /// </summary>
        /// <param name="deckName">DeckName to check against</param>
        /// <param name="noteID">NoteID to check</param>
        /// <exception cref="ArgumentException">Thrown, if the note is not found for given deck</exception>
        private void CheckNoteID(string deckName, long noteID)
        {
            try
            {
                dbContexts[deckName].Notes.Single(entry => entry.ID == noteID);
            }
            catch (InvalidOperationException)
            {
                throw new ArgumentException("Invalid noteID");
            }
        }

        /// <summary>
        /// Checks if the given CardID exists for the given deck
        /// </summary>
        /// <param name="deckName">DeckName to check against</param>
        /// <param name="cardID">CardID to check</param>
        /// <exception cref="ArgumentException">Thrown, if the card is not found for given deck</exception>
        private void CheckCardID(string deckName, long cardID)
        {
            try
            {
                dbContexts[deckName].Cards.Single(entry => entry.ID == cardID);
            }
            catch (InvalidOperationException)
            {
                throw new ArgumentException("Invalid cardID");
            }
        }

        /// <summary>
        /// Checks if the count of fields given matches the one in the given model
        /// </summary>
        /// <param name="deckName">DeckName to check against</param>
        /// <param name="modelID">ModelID to check against</param>
        /// <param name="fieldCount">FieldCount of given fields</param>
        /// <exception cref="ArgumentException">Thrown, when counts do not match</exception>
        private void CheckFieldCount(string deckName, long modelID, int fieldCount)
        {
            if (dbContexts[deckName].CollectionInformation.Where(entry => entry.Models[modelID].RequiredFields.Count() == fieldCount).Count() != 1)
                throw new ArgumentException("Invalid number of fields, the number must be equal to your field count in the given model");
        }
        #endregion

        private long? GetDeckID(string deckName, Dictionary<long, Deck> decks)
        {
            foreach (var (deckID, deck) in decks)
            {
                if (deck.Name == deckName)
                    return deck.DeckID;
            }
            return null;

        }
    }
}
