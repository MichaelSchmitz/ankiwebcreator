﻿using AnkiDB.Models;
using AnkiDB.Models.JSON;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace AnkiDB.Database
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options) {
            Database.EnsureCreated();
        }

        public DbSet<Card> Cards { get; set; }
        public DbSet<CollectionInformation> CollectionInformation { get; set; }
        public DbSet<Grave> Graves { get; set; }
        public DbSet<Note> Notes { get; set; }
        public DbSet<Review> ReviewHistory { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<CollectionInformation>().Property(t => t.Configuration).HasConversion(v => JsonSerializer.Serialize(v, typeof(Configuration), null),
                v => JsonSerializer.Deserialize<Configuration>(v, null));
            builder.Entity<CollectionInformation>().Property(t => t.Decks).HasConversion(v => JsonSerializer.Serialize(v, typeof(Dictionary<long, Deck>), null),
                v => JsonSerializer.Deserialize<Dictionary<long, Deck>>(v, null));
            builder.Entity<CollectionInformation>().Property(t => t.DeckConfigurations).HasConversion(v => JsonSerializer.Serialize(v, typeof(Dictionary<long, DeckConfiguration>), null),
                v => JsonSerializer.Deserialize<Dictionary<long, DeckConfiguration>>(v, null));
            builder.Entity<CollectionInformation>().Property(t => t.Models).HasConversion(v => JsonSerializer.Serialize(v, typeof(Dictionary<long, Model>), null),
                v => JsonSerializer.Deserialize<Dictionary<long, Model>>(v, null));
        }

        public async override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            var modifiedEntities = ChangeTracker.Entries()
                   .Where(p => p.State == EntityState.Modified).ToList();

            // increment UpdateSequenceNumber for all entities
            // set modificationTime
            foreach (var change in modifiedEntities)
            {
                if (change.Entity is IUsnMod) {
                    var UpdateSequenceNumberField = change.Entity.GetType().GetProperty("UpdateSequenceNumber");
                    UpdateSequenceNumberField.SetValue(change.Entity, ((int) UpdateSequenceNumberField.GetValue(change.Entity)) + 1);
                    var ModificationTimeField = change.Entity.GetType().GetProperty("ModificationTime");
                    ModificationTimeField.SetValue(change.Entity, DateTimeOffset.Now.ToUnixTimeMilliseconds());
                }
            }
            return await base.SaveChangesAsync(cancellationToken);
        }
    }
}
