# AnkiWebCreator
This project enables you to create and manage [Anki](https://apps.ankiweb.net/) decks and notes via an WebUI.

Its written in C# utilizing Blazor ServerSide WebApps.

The needed package structure and database schemas are written with EFCore and are separated into a class library [AnkiDB](AnkiDB) to provide a high level interface which might be useful for someone else.

A docker image with the built version is also available [here](https://hub.docker.com/repository/docker/bl4d3s/anki-web-creator).
Run it with the following command.
`docker run -it --name anki-web-creator -v $(pwd)/appsettings.json:/app/appsettings.json:ro -v $(pwd)/decks/:/app/decks/ --restart=always bl4d3s/anki-web-creator`

## Features
- Create decks
- Manage deck settings
- Create notes (and automatically cards for those)
- Add animated stroke order in an additional field, if enabled (for Kanjis)
- Add audio for Kanjis with readings automatically
- Create and download APKGs for an convenient import into Anki
- Site is protected with single user (password hash and username configurable in appsettings.json)

# Credits
- Database schema: [AnkiDroid GitHub](https://github.com/ankidroid/Anki-Android/wiki/Database-Structure)
- Kanji Stroke SVGs, Audio files and used Kanji Font: [Kanji-alive](https://github.com/kanjialive/kanji-data-media)
- Generation of animated SVGs out of single frames: [svgasm](https://github.com/tomkwok/svgasm)

This project is licensed under GNU GPLv3.