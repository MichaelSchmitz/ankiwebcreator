using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using AnkiWebCreator.Data;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

namespace AnkiWebCreator.Pages.Authentication
{
    public class LoginModel : PageModel
    {
        private readonly SiteUser siteUser;

        public LoginModel(IConfiguration configuration)
        {
            siteUser = configuration.GetSection("SiteUser").Get<SiteUser>();
        }

        public async Task<IActionResult> OnGetAsync(string paramUsername, string paramPassword)
        {
            string returnUrl = Url.Content("~/");
            try
            {
                // Clear the existing external cookie
                await HttpContext
                    .SignOutAsync(
                    CookieAuthenticationDefaults.AuthenticationScheme);
            }
            catch { }
            PasswordHasher<string> hasher = new();
            if (paramUsername == siteUser.Name && hasher.VerifyHashedPassword(siteUser.Name, siteUser.Password, paramPassword) != PasswordVerificationResult.Failed)
            {

                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, paramUsername),
                    new Claim(ClaimTypes.Role, "Administrator"),
                };
                var claimsIdentity = new ClaimsIdentity(
                    claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var authProperties = new AuthenticationProperties
                {
                    IsPersistent = true,
                    RedirectUri = Request.Host.Value
                };
                try
                {
                    await HttpContext.SignInAsync(
                    CookieAuthenticationDefaults.AuthenticationScheme,
                    new ClaimsPrincipal(claimsIdentity),
                    authProperties);
                }
                catch { }
                return LocalRedirect(returnUrl);
            }
            return LocalRedirect($"{returnUrl}?message=Invalid credentials");
        }
    }
}
