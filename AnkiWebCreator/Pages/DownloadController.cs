﻿using AnkiDB.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;

namespace AnkiWebCreator.Pages
{
    [ApiController, Route("[controller]")]
    public class DownloadController : ControllerBase
    {
        private readonly AnkiService ankiService;

        public DownloadController(AnkiService ankiService)
        {
            this.ankiService = ankiService;
        }

        [HttpGet, Route("{name}")]
        public ActionResult Get(string name)
        {
            try
            {
                ankiService.CreateOrOverwriteApkgFile(name.Substring(0, name.Length-5), "apkgs");
                var stream = new FileStream($"apkgs/{name}", FileMode.Open);

                var result = new FileStreamResult(stream, "application/octet-stream");
                result.FileDownloadName = name;
                return result;
            }
            catch (InvalidOperationException)
            {
                return BadRequest();
            }
            catch (FileNotFoundException)
            {
                return NotFound();
            }
        }
    }
}
