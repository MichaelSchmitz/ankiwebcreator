﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace AnkiWebCreator.Shared
{
    public class OneOfFieldsAttribute : CompareAttribute
    {
        public OneOfFieldsAttribute(string otherProperty) : base(otherProperty) { }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value is string field)
            {
                var property = validationContext.ObjectType.GetProperty(OtherProperty);
                if (property == null)
                    throw new ArgumentException($"Property {OtherProperty} not found");
                var fields = (property.GetValue(validationContext.ObjectInstance) as string ?? "").Split(';').ToList();
                if (fields.Contains(field))
                    return null;
            }
            return new ValidationResult("The sort field needs to be included in the list of fields",
            new[] { validationContext.MemberName });
        }
    }
}
