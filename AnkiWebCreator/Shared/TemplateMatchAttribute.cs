﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace AnkiWebCreator.Shared
{
    public class TemplateMatchAttribute : CompareAttribute
    {
        public TemplateMatchAttribute(string otherProperty) : base(otherProperty) { }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
                return null;
            if (value is string template)
            {
                var property = validationContext.ObjectType.GetProperty(OtherProperty);
                if (property == null)
                    throw new ArgumentException($"Property {OtherProperty} not found");
                var fields = (property.GetValue(validationContext.ObjectInstance) as string ?? "").Split(';');
                foreach (var field in fields)
                {
                    template = Regex.Replace(template, $"{{([^{{}}]*?:)?{field}}}", "");
                }
                if (Regex.Matches(template, "{.*?}").Count == fields.Length)
                    return null;
            }
            return new ValidationResult("The template needs matching numbers of fields",
            new[] { validationContext.MemberName });
        }
    }
}
