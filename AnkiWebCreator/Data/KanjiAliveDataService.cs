﻿using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

namespace AnkiWebCreator.Data
{
    public class KanjiAliveDataService
    {
        private static string pathStrokes = Path.Combine("decks", "kanjiAliveStrokes.json");
        private static string pathAudio = Path.Combine("decks", "kanjiAliveAudio.json");
        private HashSet<string> useStrokes;
        private HashSet<string> useAudio;

        public KanjiAliveDataService()
        {
            ReadSets();
        }

        public async Task EnableKanjiStrokes(string deckName)
        {
            if (string.IsNullOrWhiteSpace(deckName) || useStrokes.Contains(deckName))
                return;
            useStrokes.Add(deckName);
            await WriteSet(useStrokes, pathStrokes);
        } 

        public async Task DisableKanjiStrokes(string deckName)
        {
            if (string.IsNullOrWhiteSpace(deckName) || !useStrokes.Contains(deckName))
                return;
            useStrokes.Remove(deckName);
            await WriteSet(useStrokes, pathStrokes);
        }

        public async Task EnableKanjiAudio(string deckName)
        {
            if (string.IsNullOrWhiteSpace(deckName) || useAudio.Contains(deckName))
                return;
            useAudio.Add(deckName);
            await WriteSet(useAudio, pathAudio);
        }

        public async Task DisableKanjiAudio(string deckName)
        {
            if (string.IsNullOrWhiteSpace(deckName) || !useAudio.Contains(deckName))
                return;
            useAudio.Remove(deckName);
            await WriteSet(useAudio, pathAudio);
        }

        public bool IsEnabledStrokes(string deckName)
        {
            if (string.IsNullOrWhiteSpace(deckName))
                return false;
            return useStrokes.Contains(deckName);
        }

        public bool IsEnabledAudio(string deckName)
        {
            if (string.IsNullOrWhiteSpace(deckName))
                return false;
            return useAudio.Contains(deckName);
        }

        public string GetStrokesFileName(string kanji)
        {
            var file = KanjiAliveMapping.KanjiStrokes.GetValueOrDefault(kanji);
            if (file == null)
                return "";
            else
                return $"{file}.svg";
        }

        public List<string> GetAudioFileNames(string kanjiReading)
        {
            var files = KanjiAliveMapping.KanjiAudio.GetValueOrDefault(kanjiReading);
            if (files == default)
                return new();
            else
                return files;
        }

        private void ReadSets()
        {
            if (File.Exists(pathStrokes))
            {
                var json = File.ReadAllText(pathStrokes);
                var parsed = JsonSerializer.Deserialize<HashSet<string>>(json);
                useStrokes = parsed;
            } else
            {
                useStrokes = new();
            }
            if (File.Exists(pathAudio))
            {
                var json = File.ReadAllText(pathAudio);
                var parsed = JsonSerializer.Deserialize<HashSet<string>>(json);
                useAudio = parsed;
            }
            else
            {
                useAudio = new();
            }
        }

        private async Task WriteSet(HashSet<string> set, string path)
        {
            using var fileStream = File.Create(path);
            await JsonSerializer.SerializeAsync(fileStream, set);
            await fileStream.DisposeAsync();
        }
    }
}
