﻿using AnkiDB.Models;
using AnkiDB.Services;
using System;
using System.Linq;
using System.Threading.Tasks;
using static AnkiDB.Models.Card;

namespace AnkiWebCreator.Data
{
    public static class AnkiNote
    {
        public static async Task<bool> CreateIfNew(this AnkiService ankiService, string deckName, Note note, long deckID, long newCardsCount)
        {
            var old_notes = await ankiService.GetNotes(deckName);
            var old_note = old_notes.SingleOrDefault(_note => _note.ID == note.ID);
            if (old_note == default)
            {
                var cards = await ankiService.GetCards(deckName);
                var last_card_id = cards.Count > 0 ? cards.Max(card => card.ID) : 0;
                ankiService.CreateNoteAndCards(deckName, note, deckID, newCardsCount * 2 + last_card_id + 1);
                return true;
            }
            return false;
        }

        public static void CreateNoteAndCards(this AnkiService ankiService, string deckName, Note note, long deckID, long idBase = 0)
        {
            var id = idBase == 0 ? DateTimeOffset.Now.ToUnixTimeMilliseconds() : idBase;
            ankiService.AddNote(deckName, note);
            var front = new Card()
            {
                ID = id,
                NoteID = note.ID,
                DeckID = deckID,
                Type = CardType.New,
                Queue = QueueType.New,
                Due = note.ID,
                Interval = default,
                EaseFactor = default,
                Repetitions = default,
                Relapses = default,
                Left = default,
                Flags = default,
                UpdateSequenceNumber = 0,
                Data = "",
                Ordinal = 0
            };
            var back = new Card()
            {
                ID = id + 1,
                NoteID = note.ID,
                DeckID = deckID,
                Type = CardType.New,
                Queue = QueueType.New,
                Due = note.ID,
                Interval = default,
                EaseFactor = default,
                Repetitions = default,
                Relapses = default,
                Left = default,
                Flags = default,
                UpdateSequenceNumber = 0,
                Data = "",
                Ordinal = 1
            };
            ankiService.AddCard(deckName, front);
            ankiService.AddCard(deckName, back);
        }
    }
}
