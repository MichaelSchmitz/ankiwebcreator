﻿using AnkiDB.Models.JSON;
using AnkiWebCreator.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json;
using static AnkiDB.Models.JSON.Configuration;
using static AnkiDB.Models.JSON.Model;

namespace AnkiWebCreator.Data
{
    public class AnkiConfigInputModel
    {
        private long modelID;
        private long deckID;
        private long configurationID;

        public AnkiConfigInputModel(long modelID = -1, long deckID = -1, long configurationID = -1)
        {
            this.modelID = modelID == -1 ? DateTimeOffset.Now.ToUnixTimeMilliseconds() : modelID;
            this.deckID = deckID == -1 ? DateTimeOffset.Now.ToUnixTimeMilliseconds() + 1 : deckID;
            this.configurationID = configurationID == -1 ? DateTimeOffset.Now.ToUnixTimeMilliseconds() + 2 : configurationID;
        }

        [Required]
        public string Name { get; set; }

        #region Model
        public string CSS { get; set; } = ".card {\n font-family: arial;\n font-size: 20px;\n text-align: center;\n color: black;\n background-color: white;\n}";
        public string LatexPostExpression { get; set; } = "\\end{document}";
        public string LatexPreExpression { get; set; } = "\\documentclass[12pt]{article}\n\\special{papersize=3in,5in}\n\\usepackage{amssymb,amsmath}\n\\pagestyle{empty}\n\\setlength{\\parindent}{0in}\n\\begin{document}\n";
        public string Tags { get; set; }
        public string Font { get; set; } = "Arial";
        public int FontSize { get; set; } = 12;

        [Required]
        [OneOfFields(nameof(Fields))]
        public string SortField { get; set; }

        [Required]
        [RegularExpression("(.+;.+)", ErrorMessage = "At least two fields are needed (seperated by ;)")]
        public string Fields { get; set; }

        [Required]
        [TemplateMatch(nameof(Fields))]
        [RegularExpression(".+|.+", ErrorMessage = "Front and back side separated by |")]
        public string QuestionAnswerTemplate { get; set; }
        [TemplateMatch(nameof(Fields))]
        [RegularExpression("(.+|.+)?", ErrorMessage = "Front and back side separated by |")]
        public string QuestionAnswerTemplateReversed { get; set; }
        #endregion

        #region Configuration
        public OrderNewSpread OrderNewAndReview { get; set; }
        public int TimeForLookahead { get; set; }
        public int TimeForShowingReviewedCards { get; set; }
        public int MaximumDueForNewCard { get; set; }
        public bool ShowNextReviewTime { get; set; } = true;
        public bool ShowRemainingCardCount { get; set; } = true;
        public bool SortBackwards { get; set; } = true;
        public bool AddToCurrent { get; set; } = true;
        #endregion

        #region Deck
        public bool Collapsed { get; set; } = true;
        public string Description { get; set; } = "";
        #endregion

        #region DeckConfiguration
        public bool Autoplay { get; set; } = true;
        public bool ShowTimer { get; set; } = true;
        public bool ReplayOnAnswer { get; set; } = true;
        public int TimerMaximum { get; set; } = 60;
        #endregion

        public Configuration GetConfiguration()
        {
            return new Configuration
            {
                CurrentDeckID = deckID,
                ActiveDecks = new() { deckID },
                OrderNewAndReview = OrderNewAndReview,
                TimeForLookahead = TimeForLookahead,
                TimeForShowingReviewedCards = TimeForShowingReviewedCards,
                ShowNextReviewTime = ShowNextReviewTime,
                ShowRemainingCardCount = ShowRemainingCardCount, 
                MaximumDueForNewCard = MaximumDueForNewCard,
                CurrentModelID = modelID.ToString(),
                SortType = "noteFld",
                SortBackwards = SortBackwards,
                AddToCurrent = AddToCurrent,
                NewBury = true,
            };
        }

        public Dictionary<long, Model> GetModels(bool addHintForKanjiStrokes, bool addFieldForKanjiAudio)
        {
            var fields = Fields.Split(';').ToList();
            if (addHintForKanjiStrokes)
                fields.Add("KanjiStrokes");
            if (addFieldForKanjiAudio)
                fields.Add("KanjiAudio");
            return new() {
                {
                    modelID,
                    new Model
                    {
                        CSS = CSS,
                        DeckID = deckID,
                        Fields = GetFields(fields),
                        ModelID = modelID,
                        LatexPostExpression = LatexPostExpression,
                        LatexPreExpression = LatexPreExpression,
                        ModificationTime = DateTimeOffset.Now.ToUnixTimeMilliseconds(),
                        RequiredFields = JsonSerializer.Deserialize<List<object>>("[ [ 0, \"any\", [ 0 ] ], [ 1, \"any\", [ 0 ] ] ]"),
                        ModelName = $"{Name} by AnkiWebCreator",
                        SortField = fields.IndexOf(SortField),
                        Tags = Tags?.Split(';').ToList() ?? new List<string>(),
                        Templates = GetTemplates(addHintForKanjiStrokes, addFieldForKanjiAudio),
                        UpdateSequenceNumber = -1
                    }      
                }
            };
        }

        public Dictionary<long, Deck> GetDecks()
        {
            return new()
            {
                {
                    deckID,
                    new Deck
                    {
                        DeckID = deckID,
                        Name = Name,
                        ExtendedReviewCardLimit = 10,
                        ExtendedNewCardLimit = 10,
                        UpdateSequenceNumber = -1,
                        Collapsed = Collapsed,
                        NewToday = new int[] { 365, 0 },
                        RevisionsToday = new int[] { 365, 0 },
                        LearnedToday = new int[] { 365, 0 },
                        TimeToday = new int[] { 365, 0 },
                        BrowserCollapsed = Collapsed,
                        ConfigurationID = configurationID,
                        LastModificationTime = DateTimeOffset.Now.ToUnixTimeMilliseconds(),
                        Description = Description
                    }
                }
            };
        }

        public Dictionary<long, DeckConfiguration> GetDeckConfigurations()
        {
            return new()
            {
                {
                    deckID,
                    new DeckConfiguration
                    {
                        Autoplay = Autoplay,
                        DeckID = deckID,
                        Name = $"{Name} by AnkiWebCreator",
                        LastModificationTime = DateTimeOffset.Now.ToUnixTimeMilliseconds(),
                        TimerMaximum = TimerMaximum,
                        UpdateSequenceNumber = 0,
                        ShowTimer = ShowTimer ? 0 : 1,
                        ReplayAudioOnAnswer = ReplayOnAnswer,
                        NewCardConfiguration = new DeckConfiguration.NewCardConfig
                        {
                            Bury = false,
                            StepDelays = new List<double> {1, 10},
                            InitialEaseFactor = 2500,
                            DelayIntervals = new List<int> { 1, 4, 7},
                            ShowOrder = DeckConfiguration.NewCardConfig.NewShowOrder.Due,
                            MaximumPerDay = 20
                        },
                        ReviewConfiguration = new DeckConfiguration.ReviewConfig
                        {
                            Bury = false,
                            AddOnEasyPress = 1.3,
                            RandomIntervalBorder = 0.05,
                            IntervalMultiplicationFactor = 1,
                            MaximumReviewInterval = 36500,
                            MaximumPerDay = 200
                        },
                        LapseConfiguration = new DeckConfiguration.LapseConfig
                        {
                            Delays = new List<double> { 10 },
                            LeechAction = DeckConfiguration.LapseConfig.LapseLeechAction.Suspend,
                            LeechFails = 8,
                            MinimumInterval = 1,
                            Multiplier = 0
                        }
                    }
                }

            };
        }
        private List<Field> GetFields(List<string> fields)
        {
            var count = 0;
            var list = new List<Field>();
            foreach (var field in fields)
            {
                list.Add(new()
                {
                    Ordinal = count++,
                    Name = field,
                    Font = Font,
                    FontSize = FontSize,
                    Media = new()
                });
            }
            return list;
        }

        private List<Template> GetTemplates(bool addKanjiStrokesField, bool addFieldForKanjiAudio)
        {
            var tmp = QuestionAnswerTemplate.Split("|");
            var questionTemplate = tmp[0] + (addKanjiStrokesField ? "{{hint:KanjiStrokes}}" : "") + (addFieldForKanjiAudio ? "{{KanjiAudio}}" : "");
            var answerTemplate = tmp[1];
            var questionTemplateReversed = answerTemplate;
            answerTemplate = "{{FrontSide}} <hr id=answer>" + answerTemplate;
            var answerTemplateReversed = "{{FrontSide}} <hr id=answer>" + questionTemplate;
            if (!string.IsNullOrWhiteSpace(QuestionAnswerTemplateReversed))
            {
                tmp = QuestionAnswerTemplateReversed.Split("|");
                questionTemplateReversed = tmp[0];
                answerTemplateReversed = "{{FrontSide}} <hr id=answer>" + tmp[1] + (addKanjiStrokesField ? "{{hint:KanjiStrokes}}" : "") + (addFieldForKanjiAudio ? "{{KanjiAudio}}" : "");
            }
            return new()
            {
                new()
                {
                    Name = "Forward",
                    AnswerFormat = answerTemplate,
                    QuestionFormat = questionTemplate,
                    BrowserAnswerFormat = answerTemplate,
                    BrowserQuestionFormat = questionTemplate,
                    Ordinal = 0
                },
                new()
                {
                    Name = "Backward",
                    AnswerFormat = answerTemplateReversed,
                    QuestionFormat = questionTemplateReversed,
                    BrowserAnswerFormat = answerTemplateReversed,
                    BrowserQuestionFormat = questionTemplateReversed
                }
            };
        }
    }
}
