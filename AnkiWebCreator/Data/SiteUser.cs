﻿using System.ComponentModel.DataAnnotations;

namespace AnkiWebCreator.Data
{
    public class SiteUser
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
